package br.Principal;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import br.Dados.Dados;
import br.HomePage.HomePage;

public class Principal {
	protected WebDriver driver;
	protected HomePage page;
	protected Dados dados;
	
	@Before
	public void inicializar() {
		driver = new ChromeDriver() ;
		driver.manage().window().maximize();
		driver.get("http://seubarriga.wcaquino.me/login");
		page = new HomePage(driver);
		
			
		}
	@After
	public void encerrar() {
		driver.quit();
		
	}
	
}
