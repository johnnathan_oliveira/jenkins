package br.ProjetoFabrica.ProjetoFabrica;

import org.junit.Test;

import br.Principal.Principal;

public class Teste extends Principal{

	@Test
	public void Cadastro() {
		page.clickNovoUsuário();
		page.setCampoNome("Johnnathan");
		page.setEmail("Johnnathanoliver@gmail.com");
		page.setSenha("123456");
		page.cadastrar();
	}	
	@Test
	public void Login () {
		page.setEmail("Johnnathanoliver@gmail.com");
		page.setSenha("123456");
		page.logar();
		page.mensagem("Bem vindo, Johnnathan!");
	}
	@Test
	public void AdcConta() {
		Login () ;
		page.Conta();
		page.adcionar();
//		TROCAR O NOME SEMPRE Q FOR CADASTRAR
		page.setCampoNome("asuakjscbsac");
		page.salvar();
		page.mensagem("Conta adicionada com sucesso!");	
		
	}
	@Test
	public void CriarMovimentação() {
		Login ();
		page.criarMovimentp();
//		NESTE CAMPO SÓ PODE SER PREENCHIDO POR "RECEITA" "DESPESA"
		page.TipoMovimentação("Receita");
//		***********************************
		page.DataMovimentação("22/08/2018");
		page.DataPagamento("24/08/2018");
		page.Descrição("Eu quero dinheiro");
		page.Interessado("Vitor");
//		VALOR SÓ ACEITO SE DIGITADO COM "." 
		page.Valor("58.00");
		page.checkPago();
		page.salvar();
		page.mensagem("Movimentação adicionada com sucesso!");
	
	}
	@Test
	public void resumoMensal() {
		Login();
		page.resumoMensal();
//		DIGITAR ALGUM MÊS DO ANO
		page.selecionarMes("Julho");
//		DIGITAR ANO DE 2010 ATÉ 2020
		page.selecionarAno("2018");
		page.buscarResumo();
	}
	}


