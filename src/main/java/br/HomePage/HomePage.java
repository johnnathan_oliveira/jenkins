package br.HomePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.Dados.Dados;
import br.Principal.Principal;

public class HomePage extends Principal {
	public HomePage (WebDriver driver) {
		dados = new Dados(driver);
	}
	//			****CAMPOS****

	static By campoNome = By.id("nome");
	static By campoEmail = By.id("email");
	static By campoSenha = By.id("senha");
	static By NovoUsuário = By.xpath("//a[contains(text(),'Novo usuário?')]");
	static By buttonCadastrar = By.xpath("//input[@value='Cadastrar']");
	static By MensagemConfirmaçãoC = By.xpath("//div[@class='alert alert-success']");	
	static By login = By.xpath("//a[@class='navbar-brand']");
	static By buttonEntrar = By.xpath("//button[@type='submit']");
	static By MensagemConfirmaçãoL = By.xpath("//div[@class='alert alert-success']");
	static By Conta = By.xpath("//a[@class='dropdown-toggle']");
	static By adcionar = By.xpath("//a[contains(text(),'Adicionar')]");
	static By SalvarNome = By.xpath("//button[@type='submit']");
	static By Salvar = By.xpath("//div[@class='alert alert-success']");
	static By CriarMoviment = By.xpath("//a[contains(text(),'Criar Movimentação')]");
	static By TipoDeMovimentação = By.id("tipo");
	static By DataMoviment = By.id("data_transacao");
	static By DataPagamento = By.id("data_pagamento");
	static By Descrição = By.id("descricao");
	static By Interessado = By.id("interessado");
	static By Valor = By.id("valor");
	static By situaçãoPago = By.id("status_pago");
	static By situaçãoPedente = By.id("status_pendente");
	static By MensagemConfirmaçãoMovimento = By.xpath("//div[@class='alert alert-success']");
	static By ResumoMensal = By.xpath("//a[contains(text(),'Resumo Mensal')]");
	static By selecionarMes = By.id("mes");
	static By selecionarAno = By.id("ano");
	static By buscarResumoMensal = By.xpath("//input[@value='Buscar']");






	//		*************////////////////***************************

	//		CADASTRO
	public void clickNovoUsuário() {
		dados.clicarId(NovoUsuário);
	}
	public void setCampoNome(String value) {
		dados.preencherCampoName(campoNome, value);
	}
	public void setEmail(String value) {
		dados.preencherCampoName(campoEmail, value);
	}
	public void setSenha(String value) {
		dados.preencherCampoName(campoSenha, value);
	}
	public void cadastrar() {
		dados.clicarId(buttonCadastrar);
	}
	public void mensagem(String value) {
		dados.mensagemConfirmaçãoXpath(value);
	}
	//		LOGIN
	public void login (String value) {
		dados.preencherCampoName(login, value);
	}
	public void logar() {
		dados.clicarId(buttonEntrar);
	}

	//		CONTA
	public void Conta() {
		dados.clicarId(Conta);
	}
	public void adcionar() {
		dados.clicarId(adcionar);
	}
	public void salvar() {
		dados.clicarId(SalvarNome);
	}

	//		MOVIMENTAÇÃO
	public void criarMovimentp() {
		dados.clicarId(CriarMoviment);
	}
	public void TipoMovimentação(String value) {
		dados.selecionarId(TipoDeMovimentação, value);
	}
	public void DataMovimentação(String value) {
		dados.preencherCampoName(DataMoviment, value);
	}
	public void DataPagamento(String value) {
		dados.preencherCampoName(DataPagamento, value);
	}
	public void Descrição(String value) {
		dados.preencherCampoName(Descrição, value);
	}
	public void Interessado(String value) {
		dados.preencherCampoName(Interessado, value);
	}
	public void Valor(String value) {
		dados.preencherCampoName(Valor, value);
	}
	public void checkPago() {
		dados.clicarId(situaçãoPago);
	}

	public void checkPedente() {
		dados.clicarId(situaçãoPedente);
	}


	//		RESUMO MENSAL
	public void resumoMensal() {
		dados.clicarId(ResumoMensal);	
	}
	public void selecionarMes(String value) {
		dados.selecionarId(selecionarMes, value);
	}
	public void selecionarAno(String value) {
		dados.selecionarId(selecionarAno, value);
	}
	public void buscarResumo() {
		dados.clicarId(buscarResumoMensal);
	}
}	

