package br.Dados;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.Principal.Principal;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class Dados extends Principal{

	public Dados(WebDriver driver) {
		this.driver = driver;

	}
	//	****PREENCHER****
	public void preencherCampoID(By id, String value) {
		driver.findElement(id).sendKeys(value);	
	}
	public void preencherCampoXpath(By xpath, String value) {
		driver.findElement(xpath).sendKeys(value);
	}
	public void preencherCampoName(By name, String value) {
		driver.findElement(name).sendKeys(value);
	}
	//	*****CLICAR*****
	public void clicarXpath(By xpath) {
		driver.findElement(xpath).click();
	}
	public void clicarId(By id) {
		driver.findElement(id).click();
	}
	public void clicarName(By name) {
		driver.findElement(name).click();
	}
	//	*****SELECIONAR*****
	public void selecionarId(By id, String value) {
		WebElement elemento = driver.findElement(id);
		Select combo = new Select(elemento);
		combo.selectByVisibleText(value);
	}
	public void selecionarXpath(By xpath, String value) {
		WebElement elemento = driver.findElement(xpath);
		Select combo = new Select(elemento);
		combo.selectByVisibleText(value);
	}
	public void selecionarName(By name, String value) {
		WebElement elemento = driver.findElement(name);
		Select combo = new Select(elemento);
		combo.selectByVisibleText(value);
	}
	//	****MENSAGEM****
	public void mensagemConfirmaçãoXpath(String value) {
		Assert.assertEquals(value, driver.findElement(By.xpath("")).getText());
	}
	public void mensagemConfirmaçãoId(String value) {
		Assert.assertEquals(value, driver.findElement(By.id("")).getText());
	}
	public void mensagemConfirmaçãoName(String value) {
		Assert.assertEquals(value, driver.findElement(By.name("")).getText());
	}



}

